import { Entity, ManyToOne, CreateDateColumn } from 'typeorm';
import { User } from '../../users/entities/User';
import { Tweet } from './Tweet';

@Entity()
export class TweetLike {
  @ManyToOne(type => User, { primary: true, eager: true })
  public user!: User;

  @ManyToOne(type => Tweet, tweet => tweet.likes, {
    primary: true,
    eager: true,
  })
  public tweet!: Tweet;

  @CreateDateColumn({ type: 'timestamp' })
  public createdAt!: number;
}
