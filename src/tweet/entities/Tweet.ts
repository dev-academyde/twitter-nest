import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { User } from '../../users/entities/User';
import { TweetLike } from './TweetLike';
import { ObjectType, Field, ID } from 'type-graphql';

@Entity()
@ObjectType()
export class Tweet {
  @PrimaryGeneratedColumn()
  @Field(type => ID)
  public id!: number;

  @Column('text')
  @Field()
  public text!: string;

  @ManyToOne(type => User)
  @Field(type => User)
  public user!: User;

  @OneToMany(type => TweetLike, like => like.tweet)
  public likes!: Promise<TweetLike[]>;

  @CreateDateColumn({ type: 'timestamp' })
  public createdAt!: number;

  @UpdateDateColumn({ type: 'timestamp' })
  public updatedAt!: number;
}
