import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tweet } from './entities/Tweet';
import { TweetController } from './controllers/TweetController';
import { TweetService } from './services/TweetService';
import { TweetLike } from './entities/TweetLike';
import { TweetResolver } from './resolvers/TweetResolver';

@Module({
  imports: [TypeOrmModule.forFeature([Tweet, TweetLike])],
  controllers: [TweetController],
  providers: [TweetService, TweetResolver],
})
export class TweetModule {}
