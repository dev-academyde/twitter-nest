import {
  Resolver,
  Query,
  Args,
  ResolveProperty,
  Parent,
  Mutation,
} from '@nestjs/graphql';
import { Tweet } from '../entities/Tweet';
import { TweetService } from '../services/TweetService';
import { ID, Int } from 'type-graphql';
import { UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from 'src/auth/decorators/GqlAuthGuard';
import { CurrentUser } from 'src/auth/decorators/CurrentUser';
import { User } from 'src/users/entities/User';

@Resolver(Tweet)
@UseGuards(GqlAuthGuard)
export class TweetResolver {
  constructor(private readonly tweetService: TweetService) { }

  @Query(returns => Tweet, { name: 'tweet' })
  public async getTweetById(@Args({ name: 'id', type: () => ID }) id: number) {
    return await this.tweetService.getTweetById(id);
  }

  @Query(returns => [Tweet], { name: 'userTweets' })
  public async getTweetsByUserId(
    @Args({ name: 'id', type: () => ID }) id: number,
    @Args({ name: 'limit', nullable: true, type: () => Int }) limit: number,
    @Args({ name: 'offset', nullable: true, type: () => Int }) offset: number,
  ) {
    const { tweets } = await this.tweetService.getTweetsByUserId(id, {
      limit,
      offset,
    });

    return tweets;
  }

  @ResolveProperty(type => Int)
  public async likeCount(@Parent() tweet: Tweet) {
    return await this.tweetService.getLikeCountByTweetId(tweet.id);
  }

  @Mutation(returns => Tweet)
  public async likeTweet(
    @Args({ name: 'id', type: () => ID }) id: number,
    @CurrentUser() user: User,
  ) {
    await this.tweetService.likeTweetById(user, id);

    return await this.tweetService.getTweetById(id);
  }

  @Mutation(returns => Tweet)
  public async unlikeTweet(
    @Args({ name: 'id', type: () => ID }) id: number,
    @CurrentUser() user: User,
  ) {
    await this.tweetService.unlikeTweetById(user, id);

    return await this.tweetService.getTweetById(id);
  }

  @Mutation(returns => Tweet)
  public async createTweet(
    @Args({ name: 'text', type: () => String }) text: string,
    @CurrentUser() user: User,
  ) {
    return await this.tweetService.createTweet(user, {
      text,
    });
  }
}
