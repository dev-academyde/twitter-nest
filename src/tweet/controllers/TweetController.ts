import {
  Controller,
  UseGuards,
  UsePipes,
  ValidationPipe,
  Body,
  Post,
  UseInterceptors,
  ClassSerializerInterceptor,
  Get,
  Param,
  Delete,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CurrentUser } from 'src/auth/decorators/CurrentUser';
import { User } from 'src/users/entities/User';
import { CreateTweetDto } from '../dtos/CreateTweetDto';
import { TweetService } from '../services/TweetService';

@Controller()
@UseGuards(AuthGuard())
@UsePipes(new ValidationPipe({ transform: true }))
@UseInterceptors(ClassSerializerInterceptor)
export class TweetController {
  constructor(private readonly tweetService: TweetService) { }

  @Post('/tweets')
  public async createTweet(
    @CurrentUser() user: User,
    @Body() payload: CreateTweetDto,
  ) {
    return await this.tweetService.createTweet(user, payload);
  }

  @Delete('/tweets/:tweetId')
  public async deleteTweet(
    @CurrentUser() user: User,
    @Param('tweetId') tweetId: number,
  ) {
    await this.tweetService.deleteTweetById(user, tweetId);

    return {};
  }

  @Get('/users/:userId/tweets')
  public async userTweets(@Param('userId') userId: number) {
    return await this.tweetService.getTweetsByUserId(userId);
  }

  @Get('/tweets/:tweetId')
  public async getTweet(@Param('tweetId') tweetId: number) {
    return await this.tweetService.getTweetById(tweetId);
  }

  @Get('/tweets/:tweetId/likes')
  public async getLikes(@Param('tweetId') tweetId: number) {
    return {
      count: await this.tweetService.getLikeCountByTweetId(tweetId),
    };
  }

  @Post('/tweets/:tweetId/likes')
  public async likeTweet(
    @CurrentUser() user: User,
    @Param('tweetId') tweetId: number,
  ) {
    return await this.tweetService.likeTweetById(user, tweetId);
  }

  @Delete('/tweets/:tweetId/likes')
  public async dislikeTweet(
    @CurrentUser() user: User,
    @Param('tweetId') tweetId: number,
  ) {
    await this.tweetService.unlikeTweetById(user, tweetId);

    return {};
  }
}
