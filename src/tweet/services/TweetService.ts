import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { CreateTweetDto } from '../dtos/CreateTweetDto';
import { User } from '../../users/entities/User';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Tweet } from '../entities/Tweet';
import { TweetLike } from '../entities/TweetLike';

@Injectable()
export class TweetService {
  constructor(
    @InjectRepository(Tweet)
    private readonly tweets: Repository<Tweet>,
    @InjectRepository(TweetLike)
    private readonly likes: Repository<TweetLike>,
  ) {}

  public async createTweet(user: User, payload: CreateTweetDto) {
    const tweet = new Tweet();

    Object.assign(tweet, { ...payload, user });

    return await this.tweets.save(tweet);
  }

  public async getTweetsByUserId(
    userId: number,
    options?: { limit?: number; offset?: number },
  ) {
    const limit = (options && options.limit) || 10;
    const offset = (options && options.offset) || 0;

    const [tweets, count] = await this.tweets
      .createQueryBuilder('tweet')
      .leftJoinAndSelect('tweet.user', 'user')
      .where('userId = :userId', { userId })
      .offset(Number(offset))
      .limit(Number(limit))
      .getManyAndCount();

    return { tweets, count };
  }

  public async deleteTweetById(user: User, tweetId: number) {
    const tweet = await this.tweets.findOne(tweetId, { relations: ['user'] });

    if (!tweet) {
      throw new NotFoundException();
    }

    if (tweet.user.id !== user.id) {
      throw new UnauthorizedException();
    }

    // Delete all likes
    await this.likes
      .createQueryBuilder()
      .where('tweetId = :tweetId', { tweetId })
      .delete()
      .execute();

    await this.tweets.remove(tweet);
  }

  public async getTweetById(tweetId: number) {
    const tweet = await this.tweets.findOne(tweetId, { relations: ['user'] });

    if (!tweet) {
      throw new NotFoundException();
    }

    return tweet;
  }

  public async likeTweetById(user: User, tweetId: number) {
    const tweet = await this.getTweetById(tweetId);

    const like = new TweetLike();

    like.user = user;
    like.tweet = tweet;

    return await this.likes.save(like);
  }

  public async unlikeTweetById(user: User, tweetId: number) {
    await this.likes
      .createQueryBuilder()
      .where('userId = :userId', { userId: user.id })
      .delete()
      .execute();
  }

  public async getLikeCountByTweetId(tweetId: number) {
    return await this.likes
      .createQueryBuilder()
      .where('tweetId = :tweetId', { tweetId })
      .getCount();
  }
}
