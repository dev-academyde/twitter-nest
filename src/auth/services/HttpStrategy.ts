import { Strategy } from 'passport-http-bearer';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { AuthService } from './AuthService';
import { JwtService } from './JwtService';

@Injectable()
export class HttpStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly jwtService: JwtService,
    private readonly authService: AuthService,
  ) {
    super();
  }

  public async validate(token: string) {
    if (!(await this.jwtService.isValid(token))) {
      throw new UnauthorizedException();
    }

    const user = await this.authService.validateUser(token);

    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
