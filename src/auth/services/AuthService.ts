import { Injectable } from '@nestjs/common';
import { JwtService } from './JwtService';
import { Repository } from 'typeorm';
import { User } from '../../users/entities/User';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    @InjectRepository(User) private readonly users: Repository<User>,
  ) {}

  public async validateUser(token: string) {
    const data = await this.jwtService.decode<{ id: number }>(token);

    if (!data) {
      return null;
    }

    return await this.users.findOne(data.id);
  }
}
