import { CreateTokenDto } from '../dtos/CreateTokenDto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../../users/entities/User';
import { Repository } from 'typeorm';
import { sign, verify, decode } from 'jsonwebtoken';
import { compare } from 'bcrypt';
import { ForbiddenException } from '@nestjs/common';
import { ConfigService } from 'src/config/ConfigService';

export class JwtService {
  constructor(
    private readonly config: ConfigService,
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
  ) {}

  public async createToken(payload: CreateTokenDto) {
    const user = await this.usersRepository.findOne({
      where: { email: payload.email },
    });

    if (!user) {
      throw new ForbiddenException();
    }

    if (!(await compare(payload.password, user.password))) {
      throw new ForbiddenException();
    }

    return await sign({ id: user.id }, this.config.jwtSecret);
  }

  public async isValid(token: string) {
    try {
      return await Boolean(verify(token, this.config.jwtSecret));
    } catch (e) {
      return false;
    }
  }

  public async decode<D>(token: string): Promise<D | null> {
    // @ts-ignore
    return await decode(token, this.config.jwtSecret);
  }
}
