import { IsNotEmpty, IsEmail } from 'class-validator';

export class CreateTokenDto {
  @IsEmail()
  @IsNotEmpty()
  public email!: string;

  @IsNotEmpty()
  public password!: string;
}
