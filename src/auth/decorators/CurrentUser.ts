import { createParamDecorator } from '@nestjs/common';
import { get } from 'lodash';

export const CurrentUser = createParamDecorator((data, req) => {
  return req.user || get(req, '[2].req.user', null);
});
