import { Module, Global } from '@nestjs/common';
import { AuthController } from './controllers/AuthController';
import { JwtService } from './services/JwtService';
import { User } from '../users/entities/User';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from './services/AuthService';
import { HttpStrategy } from './services/HttpStrategy';
import { PassportModule } from '@nestjs/passport';
import { AuthResolver } from './resolvers/AuthResolver';

@Global()
@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    PassportModule.register({ defaultStrategy: 'bearer' }),
  ],
  controllers: [AuthController],
  providers: [JwtService, AuthService, HttpStrategy, AuthResolver],
})
export class AuthModule {}
