import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { JwtService } from '../services/JwtService';
import { CreateTokenDto } from '../dtos/CreateTokenDto';

@Controller()
@UsePipes(new ValidationPipe({ transform: true }))
export class AuthController {
  constructor(private readonly authService: JwtService) {}

  @Post('/login')
  public async login(@Body() body: CreateTokenDto) {
    const token = await this.authService.createToken(body);

    return { token };
  }
}
