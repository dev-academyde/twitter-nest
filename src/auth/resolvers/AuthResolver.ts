import { Resolver, Args, Mutation } from '@nestjs/graphql';
import { JwtService } from '../services/JwtService';

@Resolver()
export class AuthResolver {
  constructor(private readonly jwtService: JwtService) { }

  @Mutation(returns => String)
  public async createToken(
    @Args({ name: 'email', type: () => String }) email: string,
    @Args({ name: 'password', type: () => String }) password: string,
  ) {
    return await this.jwtService.createToken({ email, password });
  }
}
