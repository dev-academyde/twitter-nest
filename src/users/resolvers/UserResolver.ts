import {
  Resolver,
  Query,
  Args,
  ResolveProperty,
  Parent,
  Mutation,
} from '@nestjs/graphql';
import { User } from '../entities/User';
import { UserService } from '../services/UserService';
import { ID, Int } from 'type-graphql';
import { UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from 'src/auth/decorators/GqlAuthGuard';
import { FriendshipService } from '../services/FriendshipService';
import { CurrentUser } from 'src/auth/decorators/CurrentUser';

@Resolver(User)
@UseGuards(GqlAuthGuard)
export class UserResolver {
  constructor(
    private readonly userService: UserService,
    private readonly friendship: FriendshipService,
  ) {}

  @Query(returns => User, { name: 'user' })
  public async getUserById(@Args({ name: 'id', type: () => ID }) id: number) {
    return await this.userService.getUserById(id);
  }

  @ResolveProperty(type => Int)
  public async followerCount(@Parent() user: User) {
    const { count } = await this.friendship.getFollowers(user.id);

    return count;
  }

  @ResolveProperty(type => Int)
  public async followsCount(@Parent() user: User) {
    const { count } = await this.friendship.getFollows(user.id);

    return count;
  }

  @ResolveProperty(type => [User])
  public async followers(
    @Parent() user: User,
    @Args({ name: 'limit', nullable: true, type: () => Int }) limit: number,
    @Args({ name: 'offset', nullable: true, type: () => Int }) offset: number,
  ) {
    const { followers } = await this.friendship.getFollowers(user.id, {
      limit,
      offset,
    });

    return followers;
  }

  @ResolveProperty(type => [User])
  public async follows(
    @Parent() user: User,
    @Args({ name: 'limit', nullable: true, type: () => Int }) limit: number,
    @Args({ name: 'offset', nullable: true, type: () => Int }) offset: number,
  ) {
    const { follows } = await this.friendship.getFollows(user.id, {
      limit,
      offset,
    });

    return follows;
  }

  @Mutation(returns => User)
  public async follow(
    @Args({ name: 'id', type: () => ID }) id: number,
    @CurrentUser() user: User,
  ) {
    await this.friendship.follow(user, id);

    return await this.userService.getUserById(user.id);
  }

  @Mutation(returns => User)
  public async unfollow(
    @Args({ name: 'id', type: () => ID }) id: number,
    @CurrentUser() user: User,
  ) {
    await this.friendship.unfollow(user, id);

    return await this.userService.getUserById(user.id);
  }
}
