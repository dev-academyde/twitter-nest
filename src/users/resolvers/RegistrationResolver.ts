import { Resolver, Mutation, Args } from '@nestjs/graphql';
import { UserService } from '../services/UserService';
import { User } from '../entities/User';
import { CreateUserDto } from '../dtos/CreateUserDto';

@Resolver()
export class RegistrationResolver {
  constructor(private readonly userService: UserService) {}

  @Mutation(returns => User)
  public async createUser(@Args('input') input: CreateUserDto) {
    return await this.userService.createUser(input);
  }
}
