import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/User';
import { RegistrationController } from './controllers/RegistrationController';
import { UserService } from './services/UserService';
import { FriendshipController } from './controllers/FriendshipController';
import { UserController } from './controllers/UserController';
import { Friendship } from './entities/Friendship';
import { FriendshipService } from './services/FriendshipService';
import { UserResolver } from './resolvers/UserResolver';
import { RegistrationResolver } from './resolvers/RegistrationResolver';

@Module({
  imports: [TypeOrmModule.forFeature([User, Friendship])],
  controllers: [RegistrationController, FriendshipController, UserController],
  providers: [
    UserService,
    FriendshipService,
    UserResolver,
    RegistrationResolver,
  ],
})
export class UsersModule {}
