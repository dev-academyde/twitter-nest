import { CreateUserDto } from '../dtos/CreateUserDto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../entities/User';
import { UsernameExistsException } from '../exceptions/UsernameExistsException';
import { EmailExistsException } from '../exceptions/EmailExistsException';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
  ) {}

  private async usernameExists(username: string) {
    const result = await this.usersRepository.findOne({ where: { username } });

    return Boolean(result);
  }

  private async emailExists(email: string) {
    const result = await this.usersRepository.findOne({ where: { email } });

    return Boolean(result);
  }

  public async createUser(payload: CreateUserDto) {
    if (await this.usernameExists(payload.username)) {
      throw new UsernameExistsException(payload.username);
    }

    if (await this.emailExists(payload.email)) {
      throw new EmailExistsException(payload.email);
    }

    const user = new User();

    Object.assign(user, payload);

    return await this.usersRepository.save(user);
  }

  public async getUserById(userId: number) {
    return await this.usersRepository.findOne(userId);
  }
}
