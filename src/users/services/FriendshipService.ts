import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../entities/User';
import { Friendship } from '../entities/Friendship';
import { Injectable } from '@nestjs/common';

@Injectable()
export class FriendshipService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @InjectRepository(Friendship)
    private readonly friendship: Repository<Friendship>,
  ) {}

  public async friendshipExists(followee: User, follower: User) {
    const friendship = await this.friendship.findOne({
      where: {
        followee,
        follower,
      },
    });

    return Boolean(friendship);
  }

  public async follow(user: User, userId: number) {
    const userToFollow = await this.usersRepository.findOne(userId);

    if (user.id === userId) {
      throw new Error(`Can not follow myself`);
    }

    if (!userToFollow) {
      throw new Error(`User with id ${userId} not exists`);
    }

    if (await this.friendshipExists(user, userToFollow)) {
      throw new Error(`User already follow`);
    }

    const follower = new Friendship();

    follower.followee = user;
    follower.follower = userToFollow;

    await this.friendship.save(follower);
  }

  public async unfollow(user: User, userId: number) {
    await this.friendship
      .createQueryBuilder()
      .where('followerId = :followerId', { followerId: userId })
      .andWhere('followeeId = :followeeId', { followeeId: user.id })
      .delete()
      .execute();
  }

  public async getFollows(
    userId: number,
    options?: { limit?: number; offset?: number },
  ) {
    const limit = (options && options.limit) || 10;
    const offset = (options && options.offset) || 0;

    const [friendships, count] = await this.friendship
      .createQueryBuilder('friendship')
      .leftJoinAndSelect('friendship.follower', 'follower')
      .where('followeeId = :id', { id: userId })
      .offset(Number(offset))
      .limit(Number(limit))
      .getManyAndCount();

    return {
      follows: friendships.map(friendship => friendship.follower),
      count,
    };
  }

  public async getFollowers(
    userId: number,
    options?: { limit?: number; offset?: number },
  ) {
    const limit = (options && options.limit) || 10;
    const offset = (options && options.offset) || 0;

    const [friendships, count] = await this.friendship
      .createQueryBuilder('friendship')
      .leftJoinAndSelect('friendship.followee', 'followee')
      .where('followerId = :id', { id: userId })
      .offset(Number(offset))
      .limit(Number(limit))
      .getManyAndCount();

    return {
      followers: friendships.map(friendship => friendship.followee),
      count,
    };
  }
}
