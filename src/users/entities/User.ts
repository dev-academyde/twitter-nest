import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  BeforeInsert,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { hash } from 'bcrypt';
import { ObjectType, Field, ID } from 'type-graphql';

@Entity()
@ObjectType()
export class User {
  @PrimaryGeneratedColumn()
  @Field(type => ID)
  public id!: number;

  @Column({ unique: true })
  @Field()
  public username!: string;

  @Column()
  @Field()
  public name!: string;

  @Column()
  @Exclude()
  public password!: string;

  @Column({ unique: true })
  @Field()
  public email!: string;

  @CreateDateColumn({ type: 'timestamp' })
  public createdAt!: number;

  @UpdateDateColumn({ type: 'timestamp' })
  public updatedAt!: number;

  @BeforeInsert()
  private async hashPassword() {
    this.password = await hash(this.password, 10);
  }
}
