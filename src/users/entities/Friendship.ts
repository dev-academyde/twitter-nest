import { Entity, ManyToOne, CreateDateColumn } from 'typeorm';
import { User } from './User';

@Entity()
export class Friendship {
  @ManyToOne(type => User, { primary: true })
  public follower!: User;

  @ManyToOne(type => User, { primary: true })
  public followee!: User;

  @CreateDateColumn({ type: 'timestamp' })
  public createdAt!: number;
}
