export class UsernameExistsException extends Error {
  constructor(public readonly username: string) {
    super(`Username ${username} already exists`);
  }
}
