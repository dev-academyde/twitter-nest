export class EmailExistsException extends Error {
  constructor(public readonly email: string) {
    super(`Username ${email} already exists`);
  }
}
