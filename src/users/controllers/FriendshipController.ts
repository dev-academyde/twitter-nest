import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  ValidationPipe,
  Get,
  Param,
  Query,
  Delete,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CurrentUser } from 'src/auth/decorators/CurrentUser';
import { User } from '../entities/User';
import { FriendshipService } from '../services/FriendshipService';
import { UserPaginationDto } from '../dtos/UserPaginationDto';

@Controller()
@UseGuards(AuthGuard())
@UsePipes(new ValidationPipe({ transform: true }))
export class FriendshipController {
  constructor(private readonly friendshipService: FriendshipService) {}

  @Post('/users/:userId/followers')
  public async follow(
    @CurrentUser() user: User,
    @Param('userId') userId: number,
  ) {
    await this.friendshipService.follow(user, userId);

    return {};
  }

  @Delete('/users/:userId/followers')
  public async unfollow(
    @CurrentUser() user: User,
    @Param('userId') userId: number,
  ) {
    await this.friendshipService.unfollow(user, userId);

    return {};
  }

  @Get('/users/:id/followers')
  public async getFollowers(
    @Param('id') userId: number,
    @Query() query: UserPaginationDto,
  ) {
    return await this.friendshipService.getFollowers(userId, query);
  }

  @Get('/users/:id/follows')
  public async getFollows(
    @Param('id') userId: number,
    @Query() query: UserPaginationDto,
  ) {
    return await this.friendshipService.getFollows(userId, query);
  }
}
