import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  UseInterceptors,
  ClassSerializerInterceptor,
  BadRequestException,
} from '@nestjs/common';
import { CreateUserDto } from '../dtos/CreateUserDto';
import { UserService } from '../services/UserService';
import { UsernameExistsException } from '../exceptions/UsernameExistsException';
import { EmailExistsException } from '../exceptions/EmailExistsException';

@Controller()
@UsePipes(new ValidationPipe({ transform: true }))
@UseInterceptors(ClassSerializerInterceptor)
export class RegistrationController {
  constructor(private readonly userService: UserService) {}

  @Post('/register')
  public async register(@Body() body: CreateUserDto) {
    try {
      return await this.userService.createUser(body);
    } catch (e) {
      if (e instanceof UsernameExistsException) {
        throw new BadRequestException({
          message: [
            {
              property: 'username',
              constraints: {
                usernameExists: 'username already exists',
              },
            },
          ],
        });
      }

      if (e instanceof EmailExistsException) {
        throw new BadRequestException({
          message: [
            {
              property: 'email',
              constraints: {
                emailExists: 'email already exists',
              },
            },
          ],
        });
      }

      throw e;
    }
  }
}
