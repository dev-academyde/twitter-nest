import {
  Controller,
  UseGuards,
  UseInterceptors,
  ClassSerializerInterceptor,
  Get,
  Param,
  NotFoundException,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Repository } from 'typeorm';
import { User } from '../entities/User';
import { InjectRepository } from '@nestjs/typeorm';

@Controller()
@UseGuards(AuthGuard())
@UsePipes(new ValidationPipe({ transform: true }))
@UseInterceptors(ClassSerializerInterceptor)
export class UserController {
  constructor(
    @InjectRepository(User) private readonly users: Repository<User>,
  ) {}

  @Get('/users/:id')
  public async getUserDetail(@Param('id') userId: number) {
    const user = await this.users.findOne(userId);

    if (!user) {
      throw new NotFoundException();
    }

    return user;
  }
}
