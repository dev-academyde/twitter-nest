import { IsOptional, IsNumberString } from 'class-validator';

export class UserPaginationDto {
  @IsOptional()
  @IsNumberString()
  limit?: number;

  @IsOptional()
  @IsNumberString()
  offset?: number;
}
