import { IsNotEmpty, IsEmail, MinLength } from 'class-validator';
import { InputType, Field } from 'type-graphql';

@InputType()
export class CreateUserDto {
  @Field()
  @IsNotEmpty()
  public name!: string;

  // TODO: validate no whitespace
  @Field()
  @IsNotEmpty()
  public username!: string;

  @Field()
  @IsEmail()
  @IsNotEmpty()
  public email!: string;

  @Field()
  @IsNotEmpty()
  @MinLength(5)
  public password!: string;
}
