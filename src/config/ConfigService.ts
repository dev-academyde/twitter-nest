export class ConfigService {
  constructor(private readonly config: any) {}

  get jwtSecret() {
    return this.config.JWT_SECRET;
  }
}
